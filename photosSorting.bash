#!/usr/bin/env bash

# Copyright (C) 2023  apos
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.


#
# [Output directory]
# ├── YYYY
# │   └── Month
# │       └── YYYY-MM-DD-[time]-X
# *where X is an ascending number. If multiple photos have been taken with the same timestamp increase X.

# exiftool:			https://exiftool.org/

# TODO:
# fix bugs:

# Processing file: VID_20230623_202141150.mp4
# md5sum: ../Photos/2023/June: Is a directory <-- Hopefully it is fixed
# Tue, 15 Aug 2023 00:25:56 +0300 [OK]    Camera/VID_20230623_202141150.mp4 -> ../Photos/2023/June/2023-06-23-172211-0.mp4
# Tue, 15 Aug 2023 00:25:56 +0300 [OK]    Removed Camera/VID_20230623_202141150.mp4

#Tue, 15 Aug 2023 19:01:53 +0300 [ERROR] WORK DONE  <-- change ERROR to OK


##NOTE: exiftool -ext "*" -r -tagsfromfile "%d%f.%e.json" "-DateTimeOriginal<photoTakenTimeTimestamp" -overwrite_original -d %s "Google Photos"


#Set the paths pointing at the binary of each program:
exiftoolPath=""

#Set max threads. Higher number may result in faster proccessing of files but will cost CPU.
maxThreads=4
#Change this to disable file renaming.
renameFiles=1 #not implemented yet


declare -A date
declare -a threadID[${maxThreads}]

die() {
	printf '%s\n' "$2" >&2
	log 2 "$2"
	#printf "END LOG %s" "$(date -I)" >> "${outputDir}/photoSorting.log"
	log 1 "END LOG\n\n\n"
	exit "$1"
}

log() {
	logMessage="$(date -R)\t" #add time and date to log file
	case "$1" in
	0)
		logMessage="${logMessage}[OK]\t"
		;;
	1)
		logMessage="${logMessage}[WARN]\t"
		;;
	2)
		logMessage="${logMessage}[ERROR]\t"
		;;
	*)
		printf "FATAL ERROR"
		die 1 "Error logging"
	esac

	printf "%b\n" "$logMessage$2" >> "${outputDir}/photoSorting.log"
	printf "%b\n" "$logMessage$2"
}

#Initialize log
#printf "\n\nBEGIN LOG %s\n" "$(date -I)" >> "${outputDir}/photoSorting.log"
#log 0 "EGIN LOG"

#Check if some required programs exist
for i in exiftool; do
	a="${i}Path"
	if [[ -z "${!a}" || "${!a}" == " " ]]; then
		declare "${i}Path"="$i"
	fi
	command -v "${!a}" || die 3 "${!a} cannot be executed and this script requires it."
done


#Process arguments
while [[ "$1" ]]; do
	case "$1" in
	-o|--output)
		outputDir="${2%/}"
		[[ -d "$outputDir" ]] || die 2 "Output directory $2 either does not exist or it is not accessible!"
		log 0 "BEGIN LOG"
		shift 1
		;;
	-i|--input)
		sourceDir="${2%/}"
		[[ -d "$sourceDir" ]] || die 2 "Input directory $2 either does not exist or it is not accessible!"
		[[ "$(ls "$sourceDir")" ]] || die 2 "Input directory $2 is empty!"
		shift 1
		;;
	-h|--help|*)
		printf "Use: command -i, --input /some/dir/ -o, --output /some/dir"
		die 0 "User called for help or wrong command usage"
		;;
	esac
	shift 1
done


#Scan source directory:
#Recursively check a directory (like 'find') and save directories in an array.
#Set first directory to scan:
arr_dirs[0]="$sourceDir"

currentThreadNo=0; #helping variable to set starting

for ((i=0;i<="${#arr_dirs[@]}";i++)); do #loop as many times as the number of entries in "arr_dirs" array.

	[[ "${arr_dirs[i]}" ]] || continue; #if file does not exist, skip it. #MIGHT BE UNNECESSARY

	for fileName in "${arr_dirs[i]}"/*; do #loop all files in "arr_dirs[i]".

		#If $fileName is a directory save it to arr_dirs array:
		if [[ -d "$fileName" ]]; then
			arr_dirs+=("$fileName")
			continue
		fi

		#
		var1=1 #helping variable
		while [[ $var1 == 1 ]]; do
			for ((b=0;b<maxThreads;b++)); do

				#Check if thread is alive. If not start thread.
				if [[ ! -d "/proc/${threadID[b]}" || -z "${threadID[b]}" ]]; then
					#echo "proc ${threadID[b]} is dead"	#debugging
					currentThreadNo="$b"
					#echo "Starting thread $currentThreadNo..." #debugging
					var1=0
					break
				fi
			done
		done

		{
			sourceDir="${fileName%/*}"
			#save the filename to $fileName
			fileName="${fileName##*/}"
			[[ "$fileName" == '*' ]] && exit
			printf "\nProcessing file: %s\n" "$fileName" #debugging
			fileExtension="${fileName##*.}"

			case "$fileExtension" in
				jpg|jpeg|JPG|JPEG)
					fileExtension="jpg"
					;;
				png|PNG)
					fileExtension="png"
					;;
				gif|GIF)
					fileExtension="gif"
					;;
				mp4|MP4)
					fileExtension="mp4"
					;;
				3gp|3GP)
					fileExtension="3gp"
					;;
				avi|AVI)
					fileExtension="avi"
					;;
				mov|MOV)
					fileExtension="mov"
					;;
				*)
					log 1 "$fileName is not of a supported media file."
					exit #continue
			esac

			for i1 in "datetimeoriginal" "createdate"; do
				dateTime="$("$exiftoolPath" -s3 "-$i1" "$sourceDir/$fileName")";
				[[ -z "$dateTime" ]] || break
			done


				#Trim time from output:
				dateD="${dateTime%% *}"
				dateT="${dateTime##* }"
				breakChar=':'

			for i1 in "Year" "Month" "Day"; do
					date[$i1]="${dateD%%"$breakChar"*}" 	#save the first digits until ':'
					[[ "${#date["$i1"]}" == 1 ]] && date[$i1]="0${date["$i1"]}" # convert to double digits (eg 5-7-1990 -> 05-07-1990)
					dateD="${dateD#*"$breakChar"}"	#remove the first digits until ':'
			done

			dateT="${dateT//:/}"

			#Check if year is 4 chars long, month and day are 2 chars long
			#TO DO: Check if chars are digits and add more checks
			[[ ${#date["Year"]} == 4 && ${#date["Month"]} == 2 && ${#date["Day"]} == 2 ]]  || {
				log 2 "Wrong date format on file $sourceDir/$fileName (${date["Year"]}-${date["Month"]}-${date["Day"]}-$dateT)"
				exit #continue
			}

			case "${date["Month"]}" in
			'01')
				month='January'
				;;
			'02')
				month='February'
				;;
			'03')
				month='March'
				;;
			'04')
				month='April'
				;;
			'05')
				month='May'
				;;
			'06')
				month='June'
				;;
			'07')
				month='July'
				;;
			'08')
				month='August'
				;;
			'09')
				month='September'
				;;
			'10')
				month='October'
				;;
			'11')
				month='November'
				;;
			'12')
				month='December'
				;;

			esac


			destDir="$outputDir/${date["Year"]}/$month"
			#destDir="$outputDir/${date["Year"]}/${date["Year"]}-${date["Month"]}-${date["Day"]}/"

			#Create the destination directory:
			[[ -d "$destDir" ]] || { mkdir --parents "$destDir" || die 1 "Error creating directory $destDir"; }

			md5sumSource="$(md5sum "$sourceDir/$fileName")"
			md5sumSource="${md5sumSource%% *}"

			#Check if file exists by comparing its md5sum to that of the files in destination directory:
			fileExists=0

			if [[ -n "$(ls -A "$destDir")" ]]; then
				while read -r md5sumDest; do
					#echo $md5sumDest	#debugging
					if [[ "${md5sumDest%% *}" == "$md5sumSource" ]]; then
						log 0 "$sourceDir/$fileName($md5sumSource) already exists in ${md5sumDest##* }(${md5sumDest%% *})"
						fileExists=1
						break
					fi

				done < <(md5sum "$destDir"/*)
			fi


			#The code below writes data to disk so be careful when editing:

			#If file does not exist, copy the file to directory and remove source file:
			toBeRemoved=0
			if [[ $fileExists -eq 0 ]]; then
				var1=0
				newFileName="${date["Year"]}-${date["Month"]}-${date["Day"]}-$dateT-$var1.$fileExtension"


				while [[ -e "$destDir/$newFileName" ]]; do
					newFileName="${date["Year"]}-${date["Month"]}-${date["Day"]}-$dateT-$var1.$fileExtension"
					((var1++))
				done

				if cp --no-clobber "$sourceDir/$fileName" "$destDir/$newFileName"; then
					md5sumDest="$(md5sum "$destDir/$newFileName")"
					md5sumDest="${md5sumDest%% *}"
					if [[ "$md5sumSource" == "$md5sumDest" ]]; then
						toBeRemoved=1
						log 0 "$sourceDir/$fileName -> $destDir/$newFileName"

						else
						log 1 "md5sum mismatch: $sourceDir/$fileName($md5sumSource) ${md5sumDest##* }(${md5sumDest%% *})"
					fi
				else
					log 1 "Error copying $sourceDir/$fileName"
				fi

			else
				toBeRemoved=1
			fi

			if [[ $toBeRemoved -eq 1 ]]; then
				rm --force "$sourceDir/$fileName" && log 0 "Removed $sourceDir/$fileName"
			fi
 		} &
 		threadID[currentThreadNo]="$!"
	done
done
die 0 "WORK DONE"
